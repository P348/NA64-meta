# The `sources/` dir of NA64 metaproject

The project listed in this directory may be used or not according to particular
needs. In order of abstraction level they may be splitted in four cathegories:
back-ends, middleware, front-ends and assets.

## Back-ends

This projects have minimum of dependencies that may often be satisfied at the
system level by installing them with system package manager (like `rpm`, `yum`,
`emerge`, `aur`, etc.):

 - `castlib2` is basically a pure-python package for back-up management and
 automatic interaction with CERN's CASTOR tape-streamer service. Besides of the
 synchronization routines constituates some database ORM that may be further
 integrated into front-end.
 - The `DATE` directory usually kept empty unless one has the vendor-supplied
 archive with source codes. This back-end may further be used by `GemMonitor`
 utility or become an optional dependency for `p348-daq` software.
 - `DarkPhotonMCPhys` represents a fast modular generator for A' particle
 physics. A `Geant4` plugin in a nutshell.
 - `ext.gdml` package provides a set of extensions that are useful for managing
 the geometry described in GDML format. Besides of mandatory patch for GDML
 schema, may optionally generate python parser and C++-integration library.
 The parser itself is trictly bound to `Geant4-python` wrapper that usually can
 be installed at system level. Python modules requires the Python's
 `virtualenv` util to be installed at the system.
 - The `GenFit` project is a well-elaborated software for particle tracking.
 - `goo` library provides basic infrastructure to middleware. It supplies
 logging facility, application-as-an-object management, C++-template helpers,
 additional CMake-modules for wrapper generation, uninstall target, etc. May be
 configured with quite rudimentary amount of dependencies, but it is
 recommended to have either `bfd` or `liberty` libraries for convinient stack
 unwinding routines.
 - `na64-event-exchange` is a back-end defining the event indexing mechanics,
 and dynamic guranteed event-fetching from remote storages.
 - `na64-tools` provides a set of Geant4 routines for most of the Monte-Carlo
 models published on behalf of the NA64 collaboration. Based on `Geant4`
 package and optionally can require either a `ROOT` framework or
 vendor-specific software (MMK).
 - `p348-daq` is the mash-up repository providing `CORAL` with various patches
 provided on behalf of NA64 collaboration, the `COOOL` package and other legacy
 stuff for hardware read-out and management. Many of its routines heavily uses
 `ROOT` framework, thus this is mandatory dependency.

## Middleware

At the architectural point of view the middleware is usually based on back-ends
and defines particular interfacing logic bonding back-ends together.

 - The `afNA64` project implement `StromaV` interfaces and generic template for
 NA64 experiment defining the particular sensitive detectors, metadata format,
 detector enumeration and indexing, common analysis routines and so on.
 - The `StromaV` project porvides a "`ROOT` vs. `Geant4`" integration defining
 vast amount of back-end-independent interfaces. It heavily uses `Goo`
 back-end library, the `Geant4` framework, the `ROOT` framework and `boost`
 libraries and is well-fitted for `ext.gdml` package as a run-time environment
 for its integration libs. All the routines at `StromaV` are generic meaning
 that they can be adopted to any experiment. Introduces mandatory system
 dependency --- the "Google Protocol Buffers" facility is required.

## Front-ends

 - The `NA64EvD` project is an utility application acting as an event display,
 statistics browser, fast analysis tool, etc.
 - `sV-resources` is a resources server written on python (Flask) that provides
 centralized network access to assets and metadata.

## Assets repositories

 - `na64-geomlib` is a GDML library depicting detectors of NA64 experiment.

## Deploy on lxplus

Git should be:
`/cvmfs/cms.cern.ch/slc6_amd64_gcc493/cms/cmssw/CMSSW_8_0_0_pre5/external/slc6_amd64_gcc493/bin/git`

    $ git clone https://:@gitlab.cern.ch:8443/P348/NA64-meta.git
    $ cd NA64-meta
    $ ./deploy git-https
    $ ./deploy initialize
    $ ./deploy branches

