
#
# This dockerfile performs build of ready-to use image of the entire
# metaproject adopted to be run as an application(s). It is usually used by
# docker-compose process, but one may use it directly by providing
# ARG's required below.

FROM dr.epl.tpu.ru/physicist/hepenv-gentoo:v1

# We need celery for task management:
#ENV USE="redis yaml zeromq"
#RUN emerge dev-python/celery dev-python/redis-py

# Defines presets.build/byHosts/<host> environmental presets. Usual choices
# are: "container-[development|testing|production]"
ARG NA64META_HOST
# Defines particular build profile that is usually corresponds to purpose of
# the build. "resources" is a choice for resources server.
ARG NA64META_PROFILE
# One may wish to keep build files in a dedicated directory besides of usual
# `build/` one. The `container-build.dev` has to be a good choice.
ARG NA64META_BUILDDIR
# One may wish to keep installed files in a dedicated directory besides of
# usual `build/` one. The `container-install.dev` has to be a good choice if
# system-wide installation is not desired.
ARG NA64META_INSTALLDIR
# File where to put build/isntall logs. Useful for debugging purposes.
# Use `/tmp/na64meta_deploy.log` if in doubt.
ARG NA64META_LOG_FILE

ADD . /var/src
WORKDIR /var/src
RUN ./deploy
VOLUME /var/src

# vim: ft=dockerfile
