# ⚠ This is deprecated and is about to be removed in 2021 ⚠

# NA64 meta-project

This project sums up all the available public distros bearing the NA64-related
tools.

## TL;DR

People who aren't planning to contribute or participate in all subprojects will
probably prefer an easy start by doing:

    $ git clone https://gitlab.cern.ch/P348/NA64-meta.git
    $ ./deploy initialize-https
    $ ./deploy

This will take a while and, upon successful completion, leave things
ready to use in `./install` directory. If deployment have failed the environment
probably has an unsatisfied dependency (see "Dependencies" section at the end of
this document). In this case we recommend use our customized Docker image for
urgent production needs.

## Motivation

One need to clone this distro only if access to every project is desirable.
This situation may appear when one simultaneously deal with multiple tasks.
E.g. performing operations with MC simulation and analysis of raw data. This
repository helps to keep things summed up without influencing the particular
distros.

## Structure

Upon successful cloning a distro the following structure have to appear:
* The `sources/` dir contains all the available distributive as
[git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
* The `install/` dir should be kept empty before any installation procedures
will take place. It is dedicated to contain `usr/`, `bin/` and `share/`
directories where built binaries and assets will be installed upon a successful
build.
* The `build/` dir contains temporary build files.
* The `docker/` dir keeps Docker assets we're using for development.
* The `presets.build/` dir contains common shell snippets for building the
projects on various environments and for few common purposes (MC, analysis,
event display, etc.)

## How to get in

The start-up process may differ a little depending wether you have valid
accounts at:
    - [CERN's gitlab](https://gitlab.cern.ch), for `NA64-meta` itself, `afNA64`,
`castlib2`, `DarkPhotonMCPhys`, `na64-event-exchange`, `na64-geomlib`,
`na64-tools`, `NA64EvD` and `p348-daq`.
    - [github](https://github.com), for `GenFit`, `goo` and `StromaV`.
    - [bitbucket](https://bitbucket.org), for `ext.gdml` and `sV-resources`.

If you concerned only about few of these projects, you may perform instructions
from "TL;DR" section and then manually re-set their origins.

### Deployment for developers

Otherwise, make sure your ssh-agent won't ask you for credentials and do:

    $ git clone ssh://git@gitlab.cern.ch:7999/P348/NA64-meta.git ./na64-meta
    $ cd na64-meta
    $ ./deploy

This may take a while (5-20 mins).

## Notes about submodules

The management of submodules can be tricky sometimes. It is quite easy
to mess up this meta-project, so be accurate commiting things too often.

## Build up the things

For most of the subprojects the out-of-source CMake procedure is proposed as
an official way to build and install.

As a shortcut, the root directory of this repo contains BASH-script `./deploy`
which performs all the build and install routines in a generic Linux system.
The ready libraries, executables and assets will be installed into
`./install` directory following a classic UNIX approach with "prefixed" local
installation. One may use this script as a reference while customizing own
development environment.

## Customizing deployment procedure

The `presets.build/` dir contains subsets of shell-script for automated
deployment process. Basically, one may initiate common deployment process by
just running `./deploy` symlink from the root directory of this repository.

Sometimes people need to customize the building process to adapt it for
specific environment or to adjust options for their concerns (e.g. various
purposes like MC, tracking or only analysis).

The build presets by itself are located inside `presets.build/byHosts` and
presented as a directory structure `<hostname>/<username>[-<profile>]`, where:

* `hostname` is the host name that can be obtained by `$ hostname` shell
command. Any unknown host name or `localhost` host name will be considered
as part of `_generic/` dir. Corresponding envvar is named `HOST` and may be
overriden with `--override-host|-h` command-line argument.
* `username` is the current username that can be obtained in shell by
`$ whoami` command. If no such dir presented the `_any` token will be
taken. Corresponding envvar is named `USER` and may be overriden with
`--override-user|-u` command-line argument.
* `profile` is the current build profile. It is optional and has to be
specified with `PROFILE` environment variable. Corresponding envvar is
named `PROFILE` and may be specified as a `--profile|-p` command-line argument.

Note that any of these variable can be overriden from shell environment
by manual export (not recommended) or by prefixing command with appropriate
specification. For instance, guess we have current user "john" at
host "klaatu":

    $ USER=jane PROFILE=mc ./deploy

Will cause deployment script look-up the presets by path
`presets.build/byHosts/klaatu/jane-dev`.

`./deploy` may also take command-line arguments as listed above. _Please, note
that contrary to usual POSIX convention, they must be provided immediately
after option name delimeted with equal sign_. Like:

    $ ./deploy -u=jane --profile=mc

This shortcoming originates from rudimentary support of option-parsing
facility in BASH scripts. Sorry.

### Deployment presets structure

At the presets dir (like `presets.build/byHosts/klaatu/jane-dev`) only
following files will be considered:

* A `@utils` file providing specific pre-init settings. See `_generic/_any`
file comments for explaination.
* Any file beginning with two digits and prefix (like `03_foo` or `01_all`)
will be included directly in the shell script. It is suggested to either name
it as `01_all`, or to keep meaningful names for these scripts (like `01_goo`
indicating that this is build preset for Goo library). Such files will be
included in alphabetical order.

Files with other names will not be considered by deployment procedure.

### General recommendations for deployment presets

Each subproject has its own build options, configuration and build process.
It may be difficult to figure out options suitable for your current needs at
once, so best strategy will be probably to adapt existing presets written for
different hosts/users/profiles. Nevertheless, the variables found in `@utils`
file have to become point of your primary attention when introducing build to
new build environment. These variables defines system utils necessary to
set-up building tools --- a C/C++ compiler and common flags, CMake util, etc.

# Dependencies

For full-fledged installation project require:
- CERN's ROOT framework (>=6.04)
- CERN's Geant4 MC Framework (>=4.10.02) with corresponding python bindings
and GDML enabled.
- Expat XML parser (2.20.0 is enough)
- Boost library (>=1.60)
- Google Protocol Buffers (>=3.1)
- Python (>=2.6), its `virtualenv` package and `pip`
- SWIG (>=3.0.6)
- YAML-CPP library (>=0.5.3)

For less more detailed infrastructural description refer the
`./sources/README.md` file. It is possible to get rid of some of those
dependencies by disabling unused subprojects.

