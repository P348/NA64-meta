# Castlib2 tasks directory

This dir contains pre-defined configurations for `castlib2` daemon or stages
evaluation procedure. Any file ending with `.yml` extension will be considered
as an available task by corresponding routines.

