# A `./docker/` dir of NA64-meta project

This dir keeps neccessary files for building up few docker environments
suitable for running various tasks in frame of NA64 collaboration needs. It is
under maintaining of Tomsk group, but any side contribution will be
appreciated.

In order to build up a running Docker container one has to run:

    $ docker build -t `whoami`/na64-docker-base:v1

For installing some packages one need to provide to docker run:

    --cap-add=SYS_PTRACE

# Prerequisites

For operating with docker container we do recommend to keep all the sources
at a dedicated docker volume making changes in host environment.

    $ git clone ssh://git@gitlab.cern.ch:7999/P348/NA64-meta.git na64-meta.docker

Further, we will imply that sources are located at `na64-meta.docker/`
directory on the host. To initialize all the submodules (it is mandatory step
since metaproject itself does not contain any source files except the
management BASH script):

    $ cd na64-meta.docker
    $ ./deploy git-https
    $ ./deploy initialize

This will clone all the submodules (projects included in meta-project)
recursively.

## Applications

Obviously, any application has to be built prior to running. The question is
where to perform the build. The docker host is a good place to build binaries
when they're static and host has all the necessary libraries and utils built
as static objects as well. That requirement is obtrusive and is an obvious
overklill for the prototyping, so for development needs we have to find
an approach allowing reentrant usage: one may consider container with a shared
docker volume(s) as the build environment. We will name it "binary farm" (or
binfarm) further.

### Setting up a binary farm from `hepfarm-gentoo` image

To set up a binfarm one have to get the `hepfarm-gentoo.tar.bz2` compressed
image:

    $ bzcat hepfarm-gentoo.tar.bz2 | docker import -m "initial"

This procedure will take a while (approximately 20 minutes or more, depending
on the host performance). After execution succeeds you may see a new image
added with `$ docker images` command.

### Using binary farm to build the applications

To use the extracted image as a build environment one should launch a
container with interactive shell session. It will be a good practice to keep
source code and built stuff at the one or more docker volumes (mountpoints).
I.e. one volume containing sources and build objects and one for installed
files (we still do recommend to avoid system-wide installation).

    $ docker run --rm \
        -v $(realpath na64-meta.docker/sources):/var/src/sources:ro \
        -v $(realpath na64-meta.docker/presets.build):/var/src/presets.build:ro \
        -v $(realpath na64-meta.docker/vendor):/var/src/vendor:ro \
        -v $(realpath na64-meta.docker/build):/var/src/build \
        -v $(realpath na64-meta.docker/install):/var/src/install \
        -ti <image-id> /bin/bash

After this command succeeds and offer the interactive shell one may build
projects within binfarm environment with usual `./deploy` script invokation:
    
    # chown collector:collector
    # su collector
    $ cd /var/src
    $ ./deploy

## Tips

If you wish to store full container state in compressed file dropping its
history, run:

    $ docker export <container-id> | bzip2 > </path/to/image/file.bz2>

This is not a recommended way since getting rid of container history is
generally not a good idea, however it helps a lot when one need to make a
compact (and fast) snapshot of running container.

To decompress snapshot and import it as an image, run

    $ bzcat </path/to/image/file.bz2> | docker import -m "initial"

