#!/bin/bash
docker run -ti \
    -v /usr/portage:/usr/portage:ro \
    -v /tmp/na64-meta:/var/src \
    -v /home/crank/Statistics/CERN/na64:/var/data/na64:ro \
    -v /tmp/container-logs:/tmp/shared \
    -p 5022:22 \
    -p 5080:80 \
    -p 5005:5000 \
    -e "NA64META_HOST=development" \
    -e "NA64META_PROFILE=resources" \
    -e "CFG_sV_RESOURCES=development" \
    -e "CFG_CASTLIB2=development" \
    dr.epl.tpu.ru/physicist/hepenv-gentoo:v1 /bin/bash
