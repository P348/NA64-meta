#!/bin/bash

command_exists () {
    type "$1" &> /dev/null ;
}

REALPATH=realpath

if ! command_exists $REALPATH ; then
    REALPATH="readlink -e"
fi

BASEDIR="$($REALPATH "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )")"
: ${NA64META_BUILDDIR:=build}
: ${NA64META_INSTALLDIR:=$BASEDIR/install}
: ${VERBOSITY:="1"}

if [[ $NA64META_INSTALLDIR == "/" ]] ; then
    unset NA64META_INSTALLDIR
fi

# This script is designed for fast managing of the na64-meta repository. A set
# of useful aliases is implemented here.

if [ -t 1 ]; then
    bld=$(tput bold)
    nrml=$(tput sgr0)
    byllo='\033[1;38m'
      blu='\033[0;34m'
    bcyan='\033[1;36m'
     bred='\033[1;31m'
    green='\033[0;32m'
     bgrn='\033[1;32m'
     cyan='\033[0;36m'
    violt='\033[1;35m'
    clrclr='\033[0m'
else
      bld=''
     nrml=''
    byllo=''
      blu=''
    bcyan=''
     bred=''
    green=''
     bgrn=''
     cyan=''
    violt=''
    clrclr=''
fi
undrl="\e[4m"
helpMsg="${bld}DEPLOYMENT HELPER SCRIPT FOR NA64 METAPROJECT${nrml}

*** Usage #1:
    ${bld}\$${nrml} ./deploy [(--override-host|-h)=<${undrl}NA64META_HOST${nrml}>] \\
               [(--override-user|-u)=<${undrl}NA64META_USER${nrml}>] \\
               [(--profile|-p)=<${undrl}NA64META_PROFILE${nrml}>]

    In the first mode the script will perform a deployment procedure: it will
build all the sub-projects (inside ${bld}./$NA64META_BUILDDIR${nrml} dir) and install them at local
directory (to ${bld}$NA64META_INSTALLDIR${nrml}). During this process the script will utilize a special
shell scripts (called \"build presets\") that are structured inside the
${bld}./presets.build${nrml} }irectory. The ${undrl}NA64META_HOST${nrml}/${undrl}\
NA64META_USER${nrml}/${undrl}NA64META_PROFILE${nrml} variables may also be
overriden within shell-environment, but cmd-line args have precedence.

*** Usage #2:
    ${bld}\$${nrml} ./deploy <${undrl}COMMAND${nrml}>

    The second mode is nothing more but a delegating shortcut for minor
routines useful for developers, where COMMAND has to be one of the following
strings:
    * \"${bld}initialize${nrml}\" has to be used only once when initial metaproject cloned.
      This command will make git initialize and pull all the submodules in
      ${bld}./source${nrml} directory.
    * \"${bld}distclean${nrml}\" will remove everything from a build directory;
    * \"${bld}clean${nrml}\" will remove everything from build and install directories;
    * \"${bld}git-recent-checkout${nrml}\" will update all the subprojects to the latest
      revisions.
    * \"${bld}git-https${nrml}\" will make all the submodules to track public https origins
      instead of ssh/krb5. This command is useful for one who hasn't CERN
      account and unable to pull sources using secure connections. The former
      origins will be renamed to \"origin-secure\". No .gitmodules update will
      be performed.
    * \"${bld}git-secure${nrml}\" will make all the submodules to track secure ssh origins
      (ssh/krb5) instead of HTTP.
    * \"${bld}branches${nrml}\" will checkout branches in submodules taken from
    .gitmodules file (by default git tracks commit hashes instead of particular
    branch).

    Note about verbosity --- some of verbosity regimes are useful only for
developers who prefer to read colored output from terminal rather than
uncolored one from log file. It is hard to make shell put colored output to
file, but many developers like some fancy-shmancy highlighting the crucial
parts since it increases readability and efficiency of reading.
    =0 - (quiet) no standard log will be printed to stdout, stderr will be
        output to terminal, log file will be created and will contain
        everything.
    =1 - (default) only current executed command will be printed to stdout,
        stderr will be output to terminal, log file will be created and will
        contain everything.
    =2 - current execution command and general execution information will be
        printed out, log file will be created and will contain everything.
    =3 - script debug information, runtime information and currently executed
        command will be printed, log file will be created and will contain
        everything.
    =4 - (development, extremely loquacious) like =3, but 'make' util will be
        invoked with VERBOSE=1 causing make to print a lot of information to
        terminal.
    =5 - (development) like =3, but no log file will be created performing
        colored output to terminal.
    =6 - (development, loquacious) like =4, but no log file will be created.
    For each usecase one may to specify verbosity level and log file at the
presets modules. For those verbosity regimes where logging to file is
performed, the default one will be created at the build directory. Additionally,
for use case #1 one may override presets verbosity setting by providing the -v=?
argument.

Note: This script has always to be run from metaproject base directory.\
"

set_logfile() {
    # This function will clear NA64META_LOG_FILE variable if verbosity level is
    # set to >=5
    # It also will not change the NA64META_LOG_FILE variable if it has been
    # already set.
    if [[ $VERBOSITY -gt 4 ]] ; then
        unset NA64META_LOG_FILE
    else
        test -z $NA64META_LOG_FILE \
            && mkdir -p $BASEDIR/$NA64META_BUILDDIR/ \
            && NA64META_LOG_FILE="$BASEDIR/$NA64META_BUILDDIR/$SUBCOMMAND.`date +%Y%m`.log"
        test ! -z $NA64META_LOG_FILE \
                && [[ $VERBOSITY -gt 0 ]] \
                && echo -e "${bcyan}NOTE >${clrclr} Logging file is set to $NA64META_LOG_FILE." \
                && echo -e "${bcyan}     >${clrclr} Run \"$ tail -f $NA64META_LOG_FILE\" in " \
                && echo -e "${bcyan}     >${clrclr} another terminal if you wish to see standard output or" \
                && echo -e "${bcyan}     >${clrclr} set verbosity parameter to >1 to see it in terminal."
    fi
}

log_msg(){
    typeOfMsg=$1
    shift

    test ! -z $NA64META_LOG_FILE && \
        echo -e "`date +%Y.%m.%d-%H:%M:%S` [$NA64META_USER@$NA64META_HOST] [$$] $*" | \
            sed -r "s/\x1B\[([0-9];)?([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g" \
            >> $NA64META_LOG_FILE

    [[ $typeOfMsg == *debug* ]] && [[ $VERBOSITY -lt 2 ]] && return
    [[ $typeOfMsg == *info*  ]] && [[ $VERBOSITY -lt 1 ]] && return
    [[ $typeOfMsg == *cexec* ]] && [[ $VERBOSITY -eq 0 ]] && return

    case "$typeOfMsg" in
        debug )
        test -t 1 && echo -ne "-${blu}d${clrclr}- "
        ;;
        cexec )
        rdPath="$(relpath $BASEDIR `pwd`)"
        echo -e " ${bld}${rdPath} \$${clrclr} $*"
        return
        ;;
        info )
        test -t 1 && echo -ne "-${bcyan}i${clrclr}- "
        ;;
        warn )
        test -t 1 && echo -ne "-${byllo}W${clrclr}- "
        ;;
        err|error )
        test -t 1 && echo -ne "-${bred}E${clrclr}- "
        ;;
        * )
        # This has to never been occured on production. Notify the developer
        # that log_msg() was invoked withn unknown tag:
        echo -ne "${bred}Unknown log_msg tag occured:${clrclr} $@"
        ;;
    esac

    # print to the terminal if we have one
    test -t 1 && echo -e "$*"

    return 0
}

log_exec() {
    # Filter incoming arguments stripping ones that are relevant to logging.
    # See http://wiki.bash-hackers.org/scripting/posparams#filter_unwanted_options_with_a_wrapper_script
    # for detailed example snippet.
    options=()
    eoo=()
    while [[ $1  ]] ; do 
        if ! ((!eoo)) ; then
            case "$1" in
                --stdout-log=*)
                    STDOUT_LOG="${1#*=}"
                    shift
                ;;
                --stderr-log=*)
                    STDERR_LOG="${1#*=}"
                    shift
                ;;
                --)
                    eoo=1
                    options+=("$1")
                    shift
                ;;
                *)
                    options+=("$1")
                    shift
            esac
        else
            options+=("$1")
            shift
        fi
    done

    #eval "${options[@]}" > /dev/null

    #/bin/bash -i <(echo "$@") 1> >(tee -a $NA64META_LOG_FILE) 2> >(tee -a $NA64META_LOG_FILE >&2)
    if [ -z ${NA64META_LOG_FILE+x} ] ; then
        # Log file is not defined: just run the command
        eval ${options[@]}
    else
        # Log file is defined --- save logs considering choosen verbosity
        # level:
        if [[ $VERBOSITY -lt 5 ]] ; then
            if [[ $VERBOSITY -gt 1 ]] ; then
                eval ${options[@]} 1> >(tee -a $NA64META_LOG_FILE $STDOUT_LOG >&1) \
                                     2> >(tee -a $NA64META_LOG_FILE $STDERR_LOG >&2)
            #elif ... ?
            elif [[ $VERBOSITY -eq 0 ]] ; then
                eval ${options[@]} 1>> $NA64META_LOG_FILE 2>> $NA64META_LOG_FILE
            else  # ==1
                eval ${options[@]} 1>> $NA64META_LOG_FILE 2> >(tee -a $NA64META_LOG_FILE $STDERR_LOG >&2)
            fi
        else  # >4
            eval ${options[@]}
        fi
    fi
}

set_subcommand() {
    if [[ $SUBCOMMAND == 'deploy' ]] ; then
        SUBCOMMAND=$@
    else
        echo "Error: conflicting subcommands set: \"$SUBCOMMAND\" vs. \"$@\"."
        exit 1
    fi
}

case "$@" in
    -?|--help|help )
    echo -e "$helpMsg"
    exit 0
    ;;
esac

#
# This variables can be overriden from command line by specifying them before
# script invokation.
: ${NA64META_HOST:=`hostname`}
: ${NA64META_USER:=`whoami`}
: ${NA64META_PROFILE:=generic}
: ${SUBCOMMAND:='deploy'}
for i in "$@" ; do case $i in
    -h=*|--override-host=*)
    NA64META_HOST="${i#*=}"
    shift
    ;;
    -u=*|--override-user=*)
    NA64META_USER="${i#*=}"
    shift
    ;;
    -p=*|--profile=*)
    NA64META_PROFILE="${i#*=}"
    shift
    ;;
    -v=*|--verbosity=*)
    VERBOSITY="${i#*=}"
    ;;
    --confirm-sys-installation)
    CONFIRM_SYS_INSTAL=1
    ;;
    initialize )
    set_subcommand $i
    shift
    ;;
    branches )
    set_subcommand $i
    shift
    ;;
    distclean )
    set_subcommand $i
    shift
    ;;
    clean )
    set_subcommand $i
    shift
    ;;
    git-recent-checkout )
    set_subcommand $i
    shift
    ;;
    git-https )
    set_subcommand $i
    shift
    ;;
    git-secure )
    set_subcommand $i
    shift
    ;;
    launch )
    set_subcommand $i
    shift
    ;;
    genenv )
    set_subcommand $i
    shift
esac ; done

set_logfile

GENERIC_HOST_PRESET=_any

log_msg debug "Setting up profile \"${green}$NA64META_PROFILE${clrclr}\" for \
${green}$NA64META_USER${clrclr}@${green}$NA64META_HOST${clrclr} at \"$BASEDIR\""

SCRIPTSDIR=$BASEDIR/presets.build

#
# Look up settings preset for host
if [ -d $SCRIPTSDIR/byHosts/$NA64META_HOST ] ; then
    # Host dir found
    if [ -d $SCRIPTSDIR/byHosts/$NA64META_HOST/$NA64META_USER-$NA64META_PROFILE ] ; then
        # User's profiled presets found:
        PROFILE_DIR=$SCRIPTSDIR/byHosts/$NA64META_HOST/$NA64META_USER-$NA64META_PROFILE
    elif [ -d $SCRIPTSDIR/byHosts/$NA64META_HOST/$NA64META_USER ] ; then
        # User's unprofiled presets found:
        PROFILE_DIR=$SCRIPTSDIR/byHosts/$NA64META_HOST/$NA64META_USER
    elif [ -d $SCRIPTSDIR/byHosts/$NA64META_HOST/$GENERIC_HOST_PRESET-$NA64META_PROFILE ] ; then
        # Use profiled presets for any user on host:
        PROFILE_DIR=$SCRIPTSDIR/byHosts/$NA64META_HOST/$GENERIC_HOST_PRESET-$NA64META_PROFILE
    else
        # User's presets not found --- use default:
        PROFILE_DIR=$SCRIPTSDIR/byHosts/$NA64META_HOST/$GENERIC_HOST_PRESET
    fi
else
    # Host unknown
    if [[ $NA64META_HOST == lxplus* ]] ; then
        # That's an lxplus host:
        if [ -d $SCRIPTSDIR/byHosts/_lxplus/$NA64META_USER-$NA64META_PROFILE ] ; then
            # (lxplus) User dir with profile found
            PROFILE_DIR=$SCRIPTSDIR/byHosts/_lxplus/$NA64META_USER-$NA64META_PROFILE
        elif [ -d $SCRIPTSDIR/byHosts/_lxplus/$NA64META_USER ] ; then
            # (lxplus) User's unprofiled presets found
            PROFILE_DIR=$SCRIPTSDIR/byHosts/_lxplus/$NA64META_USER
        elif [ -d $SCRIPTSDIR/byHosts/_lxplus/$GENERIC_HOST_PRESET-$NA64META_PROFILE ] ; then
            # (lxplus) Use profiled presets for any user on host:
            PROFILE_DIR=$SCRIPTSDIR/byHosts/_lxplus/$GENERIC_HOST_PRESET-$NA64META_PROFILE
        else
            # (lxplus) User's presets not found --- use default:
            PROFILE_DIR=$SCRIPTSDIR/byHosts/_lxplus/$GENERIC_HOST_PRESET
        fi
    elif [ -d $SCRIPTSDIR/byHosts/_generic/$NA64META_PROFILE ] ; then
        # Has presets for choosen profile
        PROFILE_DIR=$SCRIPTSDIR/byHosts/_generic/$NA64META_PROFILE
    else
        PROFILE_DIR=$SCRIPTSDIR/byHosts/_generic/_any
    fi
fi

log_msg info "Using presets from ${bcyan}$PROFILE_DIR${clrclr}..."

#
# Load utils pre-init presets specifying various utils:
if [ -e $PROFILE_DIR/@utils ] ; then
    log_msg debug "Profile provides pre-init config: ${green}@utils${clrclr}"
    source $PROFILE_DIR/@utils
else
    log_msg debug "Using default pre-init config: $SCRIPTSDIR/byHosts/_generic/_any/@utils"
    source $SCRIPTSDIR/byHosts/_generic/_any/@utils
fi

#
# Load profile presets --- files beginning with two digits.
for f in $( find $PROFILE_DIR -regextype posix-extended -regex ".*/[[:digit:]]{2}_.*" -type f ) ; do
    log_msg debug "Using presets module: `basename $f`"
    source $f
done

#
# Append CMAKE_FLAGS with C/C++ compiler and flags:
if [[ ! -z "${C_COMPILER// }" ]] ; then
    log_msg debug "C compiler ........... : $C_COMPILER"
    CMAKE_FLAGS+=" -DCMAKE_C_COMPILER=$C_COMPILER "
fi

if [[ ! -z "${C_CFLAGS// }" ]] ; then
    log_msg debug "C flags customized ... : $C_CFLAGS"
    CMAKE_FLAGS+=" -DCMAKE_C_FLAGS=$C_CFLAGS "
fi

if [[ ! -z "${CXX_COMPILER// }" ]] ; then
    log_msg debug "C++ compiler ......... : $CXX_COMPILER"
    CMAKE_FLAGS+=" -DCMAKE_CXX_COMPILER=$CXX_COMPILER "
fi

if [[ ! -z "${CXX_CFLAGS// }" ]] ; then
    log_msg debug "C++ flags customized . : $CXX_CFLAGS"
    CMAKE_FLAGS+=" -DCMAKE_CXX_FLAGS=$CXX_CFLAGS "
fi

if [[ ! -z "${GIT// }" ]] ; then
    log_msg debug "Git client ........... : $GIT_EXEC"
fi
if [[ ! -z "${CMAKE_EXEC// }" ]] ; then
    log_msg debug "CMake util ........... : $CMAKE_EXEC"
fi

#
# Load generic shell routines
source "$BASEDIR/presets.build/functions.ish"

case $SUBCOMMAND in
    initialize )
    log_msg info "Will set up Git submodules..."
    initialize_submodules
    log_msg info "...submodules fetched and ready."
    exit 0
    ;;
    branches )
    log_msg info "Will set up corresponding branches..."
    setup_branches
    log_msg info "...submodules now track appropriate branches now."
    exit 0
    ;;
    distclean )
    log_msg info "Will clean interim built files..."
    distclean
    log_msg info "...distclean done."
    exit 0
    ;;
    clean )
    log_msg info "Will clean built and installed files..."
    clean_all
    log_msg info "...clean done."
    exit 0
    ;;
    git-recent-checkout )
    log_msg info "Will checking out latest modules revisions..."
    git_recent_checkout
    log_msg info "...recursive checkout done."
    exit 0
    ;;
    git-https )
    log_msg info "Will make submodules track public origins..."
    git_https
    log_msg info "...origins switched to https."
    exit 0
    ;;
    git-secure )
    log_msg info "Will make submodules track secure origins..."
    git_secure
    log_msg info "...origins switched to secure ones."
    exit 0
    ;;
    launch )
    log_msg info "Will look up and launch the installed stuff..."
    deploy && source "$BASEDIR/presets.build/launch.ish"
    log_msg info "...launch script returned execution."
    exit 0
    ;;
    genenv )
    envFile="${NA64META_INSTALLDIR}/etc/na64-env.sh"
    log_msg info "Generating environment file: $envFile..."
    generate_env_file "$envFile"
    log_msg info "...done."
    exit 0
    ;;
esac

if [ -z "${NA64META_INSTALLDIR}" ] && [ -z "${CONFIRM_SYS_INSTAL+x}" ] ; then
    log_msg info "${byllo}SYSTEM-WIDE INSTALLATION PROCEDURE WILL BE PERFORMED!${clrclr}"
    if [[ -t "0" || -p /dev/stdin && -t 1 ]] ; then
        echo "Milestone."
        read -p "Interactive terminal mode detected. Please, confirm system-wide installation (y/n): " -n 1 -r
        echo    # (optional) move to a new line
        if [[ ! $REPLY =~ ^[Yy]$ ]] ; then
            exit 0
        fi
    else
        log_msg debug "Script is running in non-interactive mode. Won't ask for confirmation."
    fi
fi

log_msg info "Will do deploy..." && deploy && log_msg info "...deployment done."

